#Angstrom image to test systemd

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"

IMAGE_PREPROCESS_COMMAND = "rootfs_update_timestamp"

IMAGE_PKGTYPE = "deb"

DISTRO_UPDATE_ALTERNATIVES ??= ""
ROOTFS_PKGMANAGE_PKGS ?= '${@base_conditional("ONLINE_PACKAGE_MANAGEMENT", "none", "", "${ROOTFS_PKGMANAGE} ${DISTRO_UPDATE_ALTERNATIVES}", d)}'

IMAGE_DEV_MANAGER   = "udev"
IMAGE_INIT_MANAGER  = "systemd"
IMAGE_LOGIN_MANAGER = "shadow"

VIRTUAL-RUNTIME_initscripts = ""

export IMAGE_BASENAME = "netsam9x25-systemd-image"

IMAGE_INSTALL += ' \
        ${ROOTFS_PKGMANAGE_PKGS} \
        packagegroup-core-boot \
	packagegroup-base \
        procps \
        openssh \
        net-tools \
        traceroute \
        iproute2 \
        iptables \
        mtd-utils \
        i2c-tools \
        f2fs-tools \
        kernel-modules \
        laureline-feed-config-opkg \
	vlan \
        tcpdump \
        strace \
'

IMAGE_FEATURES += " package-management"

inherit image

