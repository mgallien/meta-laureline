#Angstrom image to test systemd

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"

IMAGE_PREPROCESS_COMMAND = "rootfs_update_timestamp"

IMAGE_PKGTYPE = "deb"
IMAGE_ROOTFS_SIZE = "4194304"

DISTRO_UPDATE_ALTERNATIVES ??= ""
ROOTFS_PKGMANAGE_PKGS ?= '${@base_conditional("ONLINE_PACKAGE_MANAGEMENT", "none", "", "${ROOTFS_PKGMANAGE} ${DISTRO_UPDATE_ALTERNATIVES}", d)}'

IMAGE_DEV_MANAGER   = "udev"
IMAGE_INIT_MANAGER  = "systemd"

VIRTUAL-RUNTIME_initscripts = ""

export IMAGE_BASENAME = "netsam9x25-systemd-wayland-image"

IMAGE_INSTALL += " \
        ${ROOTFS_PKGMANAGE_PKGS} \
        packagegroup-basic \
	packagegroup-boot \
	packagegroup-base \
        packagegroup-core-nfs-client \
        iproute2 \
        iptables \
        mtd-utils \
        i2c-tools \
        f2fs-tools \
        kernel-modules \
        laureline-feed-config-opkg \
        vlan \
        nmap \
        alsa-tools \
        alsa-utils vorbis-tools \
        mpd vim \
        upower udisks pulseaudio pulseaudio-server weston \
"

IMAGE_FEATURES += "package-management splash"

inherit image image_types_fsl

