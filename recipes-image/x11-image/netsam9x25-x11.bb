#Angstrom image to test systemd

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"

IMAGE_PREPROCESS_COMMAND = "rootfs_update_timestamp"

IMAGE_PKGTYPE = "ipk"

DISTRO_UPDATE_ALTERNATIVES ??= ""
ROOTFS_PKGMANAGE_PKGS ?= '${@base_conditional("ONLINE_PACKAGE_MANAGEMENT", "none", "", "${ROOTFS_PKGMANAGE} ${DISTRO_UPDATE_ALTERNATIVES}", d)}'

#IMAGE_DEV_MANAGER   = "udev"
#IMAGE_INIT_MANAGER  = "systemd"

VIRTUAL-RUNTIME_initscripts = ""

export IMAGE_BASENAME = "netsam9x25-systemd-x11-image"

IMAGE_INSTALL += " \
        ${ROOTFS_PKGMANAGE_PKGS} \
        packagegroup-basic \
	packagegroup-boot \
	packagegroup-base \
        packagegroup-core-x11-sato \
        packagegroup-core-x11-xserver \
        packagegroup-core-x11 \
        packagegroup-core-x11-base \
        matchbox-session \
        matchbox-session-sato \
        qtmultimedia-examples \
        qtsmarthome \
        iproute2 \
        iptables \
        mtd-utils \
        i2c-tools \
        f2fs-tools \
        kernel-modules \
        laureline-feed-config-opkg \
        vlan \
        nmap \
        gmediarender \
        alsa-tools \
        alsa-utils \
        mpd \
        upower udisks pulseaudio pavucontrol pulseaudio-server \
"

IMAGE_FEATURES += "package-management splash"

inherit image

